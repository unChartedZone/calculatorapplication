import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Driver extends Application{

    Stage window;
    Scene scene1,calcScene;
    Button changeScene,enterButton;
    VBox openCenter,calcLayout;
    Label intro,answerLabel;
    BorderPane pane1;
    TextField inputPrompt;
    Calculator calculator;
    String input;

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;

        setOpeningScreen();
        setCalcScreen();
        //Layout for the screen where the magic happens


        window.setTitle("Calculator");
        window.setScene(scene1);
        window.show();
    }

    //Sets up the first screen
    private void setOpeningScreen() {
        intro = new Label("Welcome"); //Make a label that will appear above the button
//        intro.setId("introLabel");
        intro.getStyleClass().add("cookie");

        changeScene = new Button("Enter"); //A button that will take the user to the calculator screen
        changeScene.setOnAction(e -> window.setScene(calcScene));
        changeScene.setId("dark-blue");

        openCenter = new VBox(40); //Puts that label and button into one vbox located in the center
        openCenter.getChildren().addAll(intro, changeScene);
        openCenter.setAlignment(Pos.CENTER);

        pane1 = new BorderPane();
        pane1.setCenter(openCenter);
        pane1.setId("root");

        scene1 = new Scene(pane1,900,506);
        scene1.getStylesheets().addAll(this.getClass().getResource("Viper.css").toExternalForm());

    }

    private void setCalcScreen() {
        //Make a calculator object
        calculator = new Calculator();

        //Enter Input textfield
        inputPrompt = new TextField();
        inputPrompt.setPromptText("Enter input");

        // define width limits of the input prompt
        inputPrompt.setMinWidth(50);
        inputPrompt.setPrefWidth(50);
        inputPrompt.setMaxWidth(400);


        //Label that will display the answer
        answerLabel = new Label();
        answerLabel.setVisible(false);
        answerLabel.setId("answerLabel");
//        answerLabel.getStyleClass().add("cookie");

        //Enter button
        enterButton = new Button("Enter");
        enterButton.setOnAction(e -> decideInput());
        enterButton.setId("dark-blue");

        //Setting the layout of the calculation Screen
        calcLayout = new VBox(20);
        calcLayout.setAlignment(Pos.CENTER);
        calcLayout.getChildren().addAll(inputPrompt,enterButton,answerLabel);
        calcLayout.setPadding(new Insets(10,10,10,10));
        calcLayout.setId("root");

        calcScene = new Scene(calcLayout,900,506);
        calcScene.getStylesheets().addAll(this.getClass().getResource("Viper.css").toExternalForm());
    }

    //This method will decide whether the input entered in
    //the text box will be simple arithmetic or a question is being asked.
    private void decideInput() {
        input = inputPrompt.getText();
        input = calculator.removeSpaces(input);
        if(isInt(input.substring(0,1))) {
            runCalculator();
        }
        //That means a question was asked
        else {
            answerLabel.setText("You fuck");
            answerLabel.setVisible(true);
        }
    }

    private boolean isInt(String firstChar) {
        try {
            int num = Integer.parseInt(firstChar);
            return true;
        }catch(NumberFormatException e) {
            return false;
        }

    }

    private void runCalculator() {
        input = calculator.removeSpaces(input);
        calculator.parseInput(input);
        int answer = calculator.calculateInput();
        answerLabel.setText("Your answer is: " + answer);
        answerLabel.setVisible(true);
    }
}
