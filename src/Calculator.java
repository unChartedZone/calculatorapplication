import data_structures.*;


public class Calculator {
    private int answer;
    private LinearList<Integer> nums;
    private LinearList<Character> actions;

    public Calculator() {
        //Intialize the two linked lists
        nums = new LinearList<>();
        actions = new LinearList<>();
    }

    public String removeSpaces(String input) {
        input = input.replaceAll("\\s", "");
        return input;
    }

    public void parseInput(String input) {
        String line = input; //Input with no spaces
        char[] lineArray = line.toCharArray();
        int i = 0;
        while (lineArray.length != 0) {
            if (isAction(lineArray[i])) {
                actions.addLast(lineArray[i]);
                String stringNum = new String(lineArray, 0, i);
                int num = Integer.parseInt(stringNum);
                nums.addLast(num);
                line = line.substring(i + 1);
                lineArray = line.toCharArray();
                i = 0;
                continue;
            }
            //To get the last number
            if (i == lineArray.length - 1) {
                String stringNum = new String(lineArray);
                int num = Integer.parseInt(stringNum);
                nums.addLast(num);
                break;
            }
            i++;
        }
    }

    //Used in the method parseInput to
    //check if we have encountered an operator
    private boolean isAction(char action) {
        if (action == '+') return true;
        if (action == '-') return true;
        if (action == '*') return true;
        if (action == '/') return true;
        return false;
    }


    public int calculateInput() {
        while(nums.size() != 1) {
            if (actions.peekFirst() == '+') {
                actions.removeFirst();
                int num1 = nums.removeFirst();
                int num2 = nums.removeFirst();
                answer = num1 + num2;
                nums.addFirst(answer);
                continue;
            }
            if (actions.peekFirst() == '-') {
                actions.removeFirst();
                int num1 = nums.removeFirst();
                int num2 = nums.removeFirst();
                answer = num1 - num2;
                nums.addFirst(answer);
                continue;
            }
            if (actions.peekFirst() == '*') {
                actions.removeFirst();
                int num1 = nums.removeFirst();
                int num2 = nums.removeFirst();
                answer = num1 * num2;
                nums.addFirst(answer);
                continue;
            }
            if (actions.peekFirst() == '/') {
                actions.removeFirst();
                int num1 = nums.removeFirst();
                int num2 = nums.removeFirst();
                answer = num1/num2;
                nums.addFirst(answer);
                continue;
            }
        }
        nums.clear();
        actions.clear();
        return answer;
    }

}
